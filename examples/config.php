<?php
if(function_exists("unicode_decode"))
{
	// Unicode extension introduced in 6.0
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_STRICT);
}
elseif(defined("E_DEPRECATED"))
{
	// E_DEPRECATED introduced in 5.3
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
}
else
{
	error_reporting(E_ALL & ~E_NOTICE);
}

$config = array(
	"base_url" => "http://localhost/api_v1",
	"salt"     => "3edfcv54tghn6yujhm",
	"user"     => "admin",
	"pw"       => "1234",
	"token"    => "abcdefgh12345678"
);

$user = array(
	"username" => "Name",
	"password" => "PW"
);

if(!defined("NO_LOGIN"))
{
	$config['usertoken'] = login();
}

function login()
{
	global $user, $config;

	$user['token'] = $config['token'];
	$url           = "users/login/";
	$result        = curl($url, $code, $user, true, false);
	$result        = json_decode($result, true);

	return $result['response']['usertoken'];
}

function curl($url, &$http_code = "", $postfields = array(), $auth = true, $login = true)
{
	global $config;

	$url = $config['base_url']."/".$url;
	if(!empty($postfields))
	{
		if($auth)
		{
			$postfields['token'] = $config['token'];
		}
		if($login)
		{
			$postfields['usertoken'] = $config['usertoken'];
		}
	}
	else
	{
		$add = "&";
		if(strpos($url, "?") === false)
		{
			$add = "?";
		}

		if($auth)
		{
			$url .= $add."token=".$config['token'];
			$add = "&";
		}
		if($login)
		{
			$url .= $add."usertoken=".$config['usertoken'];
		}
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, true);

	if(!empty($config['salt']) && !empty($config['user']) && !empty($config['pw']))
	{
		curl_setopt($ch, CURLOPT_USERPWD, $config['user'].':'.sha1($config['salt'].$config['pw']));
	}

	if(!empty($postfields) && is_array($postfields))
	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	}
	else
	{
		curl_setopt($ch, CURLOPT_HTTPGET, true);
	}

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	//ATM we don't send the Codes so here's a workaround
	$decoded   = json_decode($result, true);
	$http_code = $decoded['status'];

	curl_close($ch);

	return $result;
}