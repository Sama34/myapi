<?php
require_once "config.php";

$url    = "threads/";
$result = curl($url, $code, array(), false);
$result = json_decode($result, true);
$first  = @reset($result['response']);
$first  = $first['tid'];
$count  = count($result['response'][$first]);
echo "GET \"threads\" (noAuth) ({$code}) says that we have ".count($result['response'])." threads on the board<br /><br />";

$url    = "threads/{$first}";
$result = curl($url, $code);
$result = json_decode($result, true);
echo "GET \"threads/{$first}\" (Auth) ({$code}) gives ".(count($result['response'][$first]) - $count)." more information about the thread<br /><br />";

$url               = "threads/";
$post              = array(
	"fid"     => 2,
	"subject" => "MyAPI Test",
	"message" => "This is a test of POST \"threads\" with MyAPI"
);
$result            = curl($url, $code, $post);
$result            = json_decode($result, true);
$thread            = @reset($result['response']);
$thread['message'] = $post['message'];
echo "POST \"threads\" (Auth) ({$code}) added a new thread to your forum: \"{$thread['subject']}\" (TID: {$thread['tid']})<br /><br />";

$url    = "threads/".$thread['tid'];
$post   = array(
	"tid"     => $thread['tid'],
	"message" => $thread['message']."\n\nAnd now we've edited it!"
);
$result = curl($url, $code, $post);
$result = json_decode($result, true);
echo "POST \"threads/{$thread['tid']}\" (Auth) ({$code}) has edited the thread created before<br /><br />";

$url    = "threads/delete/".$thread['tid'];
$result = curl($url, $code, array("not" => "empty"));
$result = json_decode($result, true);
echo "POST \"threads/delete/{$thread['tid']}\" (Auth) ({$code}) deleted the new thread<br /><br />";

$url    = "threads/latest/1";
$result = curl($url, $code, array(), false);
$result = json_decode($result, true);
$last   = @reset($result['response']);
echo "GET \"threads/latest/1\" (noAuth) ({$code}) says that our latest thread is named \"{$last['subject']}\" and was written by \"{$last['username']}\"<br /><br />";

$url    = "threads/posts/{$first}";
$result = curl($url, $code);
$result = json_decode($result, true);
echo "GET \"threads/posts/{$first}\" (Auth) ({$code}) says that our the thread with the TID {$first} has ".count($result['response'])." post(s)<br /><br />";