<?php
require_once "config.php";

$url    = "users/";
$result = curl($url, $code, array(), false, false);
$result = json_decode($result, true);
$count  = count($result['response'][1]);
echo "GET \"users\" (noAuth) ({$code}) says that we have ".count($result['response'])." users on the board<br /><br />";

$url     = "users/1";
$result  = curl($url, $code, array(), true, false);
$result  = json_decode($result, true);
$website = $result['response'][1]['website'];
echo "GET \"users/1\" (Auth) ({$code}) gives ".(count($result['response'][1]) - $count)." more information about you<br /><br />";

$url    = "users/1";
$post   = array(
	"uid"     => 1,
	"website" => "http://mybb.com/"
);
$result = curl($url, $code, $post, true, false);
$result = json_decode($result, true);
echo "POST \"users/1\" (Auth) ({$code}) has changed your website to \"{$result['response'][1]['website']}\"<br />PS: Your old one was \"{$website}\"<br /><br />";

$url    = "users/";
$post   = array(
	"username" => "MyAPI Test",
	"password" => "test12",
	"email"    => "test@example.com"
);
$result = curl($url, $code, $post, true, false);
$result = json_decode($result, true);
$user   = @reset($result['response']);
echo "POST \"users\" (Auth) ({$code}) added a new member to your forum: \"{$user['username']}\" (UID: {$user['uid']})<br /><br />";

$url    = "users/delete/".$user['uid'];
$result = curl($url, $code, array("not" => "empty"), true, false);
$result = json_decode($result, true);
echo "POST \"users/delete/{$user['uid']}\" (Auth) ({$code}) deleted the new member<br /><br />";


echo "<hr /><br /><br />Generating a few errors for you:<br /><br />";

$url    = "user/";
$result = curl($url, $code, array(), false, false);
$result = json_decode($result, true);
echo "GET \"user\" (noAuth) ({$code}) doesn't exists ({$result['error']['message']})<br /><br />";

$url    = "users/1";
$post   = array(
	"uid"     => 1,
	"website" => "http://mybb.com/"
);
$result = curl($url, $code, $post, false, false);
$result = json_decode($result, true);
echo "POST \"users/1\" (noAuth) ({$code}) needs Authentication ({$result['error']['message']})<br /><br />";

$url    = "users/delete/1";
$result = curl($url, $code, array("not" => "empty"), true, false);
$result = json_decode($result, true);
echo "POST \"users/delete/1\" (Auth) ({$code}) is a super admin ({$result['error']['message']})<br /><br />";