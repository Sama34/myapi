<?php
require_once "config.php";

$message = "This is a sample use of the [i]parser[/i] domain with some [b]bold[/b] text.";
$url = "parser/";
$post              = array(
	"message"	=> $message,
	"options[allow_mycode]"	=> true
);
$result     = curl($url, $code, $post);
$result     = json_decode($result, true);
$decoded	= $result['response']['message'];
echo "POST \"parser/\" (Auth) ({$code}) parsed \"{$message}\" to \"".htmlentities($decoded)."\"";