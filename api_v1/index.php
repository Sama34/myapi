<?php
/**
 * Main file for parsing incoming requests to  API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */

define('API_PATH', 'api_v1');
define('IN_API', 1);
define('IN_MYBB', 1);

require_once "./inc/init.php";
require_once MYBB_ROOT.API_PATH."/inc/class_myapi.php";

//get the actual domain/namespace/entity
$request = rtrim($_SERVER["DOCUMENT_ROOT"], '/\\')."/".trim($_SERVER["REQUEST_URI"], '/\\') ;
$request = strtr($request, array(MYBB_ROOT.API_PATH.'/' => '', '?'.$_SERVER["QUERY_STRING"] => ''));
$request = explode('/', $request);

$request_method = strtolower($_SERVER['REQUEST_METHOD']);
$request_format = strpos($_SERVER['REQUEST_URI'], 'xml') ? 'xml' : 'json';

if(file_exists(MYBB_ROOT.API_PATH."/domains/".$request[0].'.php'))
{
	if(!is_object($mybb))
	{
		if($request_format == 'json')
		{
			echo json_encode(array('status' => 500, 'error' => array('message' => 'Internal Server Error')));
		}
		exit;
	}

	$pluginlist = $cache->read("plugins");
	if(!is_array($pluginlist['active']) || !in_array("myapi", $pluginlist['active']))
	{
		if($request_format == 'json')
		{
			echo json_encode(array('status' => 500, 'error' => array('message' => 'Internal Server Error')));
		}
		exit;
	}

	require_once(MYBB_ROOT.API_PATH."/domains/".$request[0].'.php');
	$api = new $request[0]($request, $mybb, $db, $cache);
	$api->sendResponse();

	exit;
}
else
{
	if($request_format == 'json')
	{
		echo json_encode(array('status' => 404, 'error' => array('message' => 'Not Found')));
	}
	exit;
}