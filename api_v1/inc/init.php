<?php
/**
 * Init file for API v1 (trimmed down from MyBB 1.6 which is released under LGPL)
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
// Disallow direct access to this file for security reasons
if(!defined("IN_MYBB"))
{
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

if(function_exists("unicode_decode"))
{
	// Unicode extension introduced in 6.0
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_STRICT);
}
elseif(defined("E_DEPRECATED"))
{
	// E_DEPRECATED introduced in 5.3
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
}
else
{
	error_reporting(E_ALL & ~E_NOTICE);
}

if(!defined('MYBB_ROOT'))
{
	define('MYBB_ROOT', str_replace(DIRECTORY_SEPARATOR, '/', dirname(dirname(dirname(__FILE__))))."/");
}

define("TIME_NOW", time());

if(function_exists('date_default_timezone_set') && !ini_get('date.timezone'))
{
	date_default_timezone_set('GMT');
}

require_once MYBB_ROOT."inc/functions.php";

require_once MYBB_ROOT."inc/class_core.php";
$mybb = new MyBB;

if(!file_exists(MYBB_ROOT."inc/config.php"))
{
	if($request_format == 'json')
	{
		echo json_encode(array('status' => 503, 'error' => array('message' => 'MyBB is not available')));
	}
	exit;
}

// Include the required core files
require_once MYBB_ROOT."inc/config.php";
$mybb->config = & $config;

if(!isset($config['database']))
{
	if($request_format == 'json')
	{
		echo json_encode(array('status' => 503, 'error' => array('message' => 'MyBB is not available')));
	}
	exit;
}

if(!is_array($config['database']))
{
	if($request_format == 'json')
	{
		echo json_encode(array('status' => 503, 'error' => array('message' => 'MyBB is not available')));
	}
	exit;
}

require_once MYBB_ROOT."inc/db_".$config['database']['type'].".php";

switch($config['database']['type'])
{
	case "sqlite":
		$db = new DB_SQLite;
		break;
	case "pgsql":
		$db = new DB_PgSQL;
		break;
	case "mysqli":
		$db = new DB_MySQLi;
		break;
	default:
		$db = new DB_MySQL;
}

// Check if our DB engine is loaded
if(!extension_loaded($db->engine))
{
	// Throw our super awesome db loading error
	if($request_format == 'json')
	{
		echo json_encode(array('status' => 503, 'error' => array('message' => 'Database is not available')));
	}
	exit;
}

require_once MYBB_ROOT."inc/class_datacache.php";
$cache = new datacache;

// Include our base data handler class
require_once MYBB_ROOT."inc/datahandler.php";

// Connect to Database
define("TABLE_PREFIX", $config['database']['table_prefix']);
$db->connect($config['database']);
$db->set_table_prefix(TABLE_PREFIX);
$db->type = $config['database']['type'];

// Load cache
$cache->cache();

// Load Settings
if(file_exists(MYBB_ROOT."inc/settings.php"))
{
	require_once MYBB_ROOT."inc/settings.php";
}

if(!file_exists(MYBB_ROOT."inc/settings.php") || !$settings)
{
	if(function_exists('rebuild_settings'))
	{
		rebuild_settings();
	}
	else
	{
		$options = array(
			"order_by"  => "title",
			"order_dir" => "ASC"
		);

		$query = $db->simple_select("settings", "value, name", "", $options);
		while($setting = $db->fetch_array($query))
		{
			$setting['value']           = str_replace("\"", "\\\"", $setting['value']);
			$settings[$setting['name']] = $setting['value'];
		}
		$db->free_result($query);
	}
}

$settings['wolcutoff']       = $settings['wolcutoffmins'] * 60;
$settings['bbname_orig']     = $settings['bbname'];
$settings['bbname']          = strip_tags($settings['bbname']);
$settings['orig_bblanguage'] = $settings['bblanguage'];

// Fix for people who for some specify a trailing slash on the board URL
if(substr($settings['bburl'], -1) == "/")
{
	$settings['bburl'] = my_substr($settings['bburl'], 0, -1);
}

// Setup our internal settings and load our encryption key
$settings['internal'] = $cache->read("internal_settings");
if(!$settings['internal']['encryption_key'])
{
	$cache->update("internal_settings", array('encryption_key' => random_str(32)));
	$settings['internal'] = $cache->read("internal_settings");
}

$mybb->settings = & $settings;

require_once MYBB_ROOT."inc/class_plugins.php";
$plugins = new pluginSystem;

//define('FORUM_URL', "forums/{fid}");
//define('FORUM_URL_PAGED', "forum/{fid}?page={page}");
define('THREAD_URL', "threads/{tid}");
define('THREAD_URL_PAGED', "threads/{tid}?page={page}");
define('THREAD_URL_ACTION', '');
define('THREAD_URL_POST', 'posts/{pid}');
define('POST_URL', "posts/{pid}");
define('PROFILE_URL', "users/{uid}");
//define('ANNOUNCEMENT_URL', "announcements/{aid}");
//define('CALENDAR_URL', "calendars/{calendar}");
//define('CALENDAR_URL_YEAR', 'calendar-{calendar}-year-{year}');
//define('CALENDAR_URL_MONTH', 'calendar-{calendar}-year-{year}-month-{month}');
//define('CALENDAR_URL_DAY', 'calendar-{calendar}-year-{year}-month-{month}-day-{day}');
//define('CALENDAR_URL_WEEK', 'calendar-{calendar}-week-{week}');
//define('EVENT_URL', "event-{eid}");
define('INDEX_URL', "index.php");
