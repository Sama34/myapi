<?php
/**
 * Main class file for API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
class myAPI
{
	private $key;

	private $allowedIPs;
	private $allowedBasicAuth;
	private $salt;

	private $checkIPs;
	private $checkBasicAuth;

	public $per_page = 10;
	public $page = 1;
	public $start = 0;

	public $data;
	public $request;
	public $method = 'get';
	public $format = 'json';
	public $callback = '';
	public $response;
	public $status = 200;
	public $isauth = 0;
	public $nonErrorStatuses = array(200, 201, 202);

	private $no_entity = array(
		"set" => array(
			array("users", "login")
		),
		"get" => array(
			array("users", "login"),
			array("threads", "latest")
		)
	);

	/**
	 * Constructor
	 *
	 * @param                                       array     The initial request array.
	 * @param                                       MyBB      Our MyBB object.
	 * @param DB_MySQL|DB_MySQLi|DB_PgSQL|DB_SQLite Our       database object of type DB_*.
	 * @param                                       datacache Our cache object.
	 *
	 * @throws InvalidArgumentException This is thrown when an invalid DB object is passed.
	 */
	public function myAPI($request, MyBB $mybbIn, $dbIn, datacache $cacheIn)
	{
		$this->mybb = $mybbIn;

		if($dbIn instanceof DB_MySQL OR $dbIn instanceof DB_MySQLi OR $dbIn instanceof DB_PgSQL OR $dbIn instanceof DB_SQLite)
		{
			$this->db = $dbIn;
		}
		else
		{
			throw new InvalidArgumentException('$db should be a valid instance of one of the MyBB DB_* classes. $db was of type: '.get_class($dbIn));
		}

		$this->cache = $cacheIn;

		//is the API enabled?
		if($this->mybb->settings['myapi_enabled'] != 1)
		{
			$this->SetStatus(503);
			$this->sendResponse();
			exit;
		}

		//popualte API vars from mybb settings
		$this->key              = $this->mybb->settings['myapi_key'];
		$this->checkIPs         = ($this->mybb->settings['myapi_allowedips'] == '' ? 0 : $this->mybb->settings['myapi_checkips']);
		$this->allowedIPs       = ($this->mybb->settings['myapi_allowedips'] == '' ? array() : explode(',', $this->mybb->settings['myapi_allowedips']));
		$this->checkBasicAuth   = $this->mybb->settings['myapi_checkauth'];
		$this->salt             = $this->mybb->settings['myapi_salt'];
		$this->allowedBasicAuth = array($this->mybb->settings['myapi_authuser'] => $this->mybb->settings['myapi_authpwd']);

		//determine if request can even be accepted
		if(!$this->checkAllowed())
		{
			$this->sendResponse();
			exit;
		}

		$this->setRequest($request);
		$this->setMethod(strtolower($_SERVER['REQUEST_METHOD']));

		//save data
		switch($this->method)
		{
			case 'get':
				$this->setData($_GET);
				break;
			case 'post':
				$this->setData($_POST);
				break;
			case 'put':
				parse_str(file_get_contents('php://input'), $put_vars);
				$this->setData(array_merge($put_vars, $query_str));
				break;
		}

		$this->setFormat($this->data['format']);
		
		if(isset($this->data['callback']) && $this->is_valid_callback($this->data['callback']))
		{
			$this->callback = $this->data['callback'];
		}

		//determine if request is authorized to see protected data or make changes
		$this->getAuth($this->data['token']);

		//Set our variables for pagination
		if(isset($this->data['page']))
		{
			$this->page = $this->data['page'];
			unset($this->data['page']);
		}
		if($this->page < 1)
		{
			$this->page = 1;
		}

		if(isset($this->data['per_page']))
		{
			$this->per_page = $this->data['per_page'];
			unset($this->data['per_page']);
		}
		if($this->per_page < 1)
		{
			$this->per_page = 1;
		}
		if($this->per_page > 100)
		{
			$this->per_page = 100;
		}
		$this->start = ($this->page - 1) * $this->per_page;

		//if still okay then lets get started
		if($this->status == 200)
		{
			//Do we have a logged in user?
			require_once MYBB_ROOT.API_PATH.'/inc/class_session.php';
			$session = new session;
			$session->init();
			if($user = $this->getLoggedin($this->data['usertoken']))
			{
				$session->load_user($user['uid']);
			}
			else
			{
				$session->load_guest();
			}
			$this->mybb->session = & $session;

			//set namespace and entity
			if(count($this->request) == 1)
			{
				$ns		= ucfirst($this->request[0]);
			}
			elseif(count($this->request) == 2)
			{
				if($this->method == "get")
				{
					$ignore = $this->no_entity["get"];
				}
				else
				{
					$ignore = $this->no_entity["set"];
				}

				if(in_array($this->request, $ignore))
				{
					$ns = ucfirst($this->request[1]);
					$entity = "";
				}
				else
				{
					$ns     = ucfirst($this->request[0]);
					$entity = $this->request[1];
				}
			}
			else
			{
				$ns     = ucfirst($this->request[1]);
				$entity = $this->request[2];
			}

			if($entity == '' || !isset($entity))
			{
				$entity = 0;
			}

			//define namespace function based on request method
			if($this->method == 'get')
			{
				$nsfunc = 'get'.$ns;
			}
			else
			{
				$nsfunc = 'set'.$ns;

				//can't update/modify without auth
				if($this->isauth != 1)
				{
					$this->setStatus(401);
				}

			}

			//if valid namespace function, call it
			if(method_exists($this, $nsfunc))
			{
				//but only if no others errors happened
				if($this->getStatus() == 200)
				{
					$this->setResponse($this->$nsfunc($entity));
				}
			}
			else //send a 404
			{
				$this->setStatus(404);
			}
		}

		//override any response with error code
		if(!in_array($this->status, $this->nonErrorStatuses))
		{
			$this->setResponse(array('status' => $this->status, 'error' => array('message' => $this->getStatusMessage($this->status))));
		}

	}

	/**
	 * Determines if requesting host, ip or agent is allowed to access the API
	 * @return boolean True if allowed access to API
	 */
	public function checkAllowed()
	{
		//check if IP is allowed
		if($this->checkIPs && !in_array($_SERVER['REMOTE_ADDR'], $this->allowedIPs))
		{
			$this->setStatus(401);
			$this->setResponse(array('message' => 'Requesting IP not allowed to connect'));

			return false;
		}

		//check if auth is allowed
		if($this->checkBasicAuth)
		{
			if(sha1($this->salt.$this->allowedBasicAuth[$_SERVER['PHP_AUTH_USER']]) != $_SERVER['PHP_AUTH_PW'])
			{
				$this->setStatus(401);
				$this->setResponse(array('message' => 'Invalid API username or password'));

				return false;
			}
		}

		return true;
	}

	/**
	 * Defines if request is authorized to return protected data or modify data
	 *
	 * @param string The access token passed by the request
	 */
	public function getAuth($token = '')
	{
		//check if token/key match
		if($token == $this->key && $token != '')
		{
			$this->isauth = 1;
		}
		else
		{
			$this->isauth = 0;
		}
	}

	public function getLoggedin($token = 0)
	{
		if(strlen($token) != 50)
		{
			return false;
		}

		//Get the session, just for 1 week
		$where = "token='".$this->db->escape_string($token)."' AND ip='{$_SERVER['REMOTE_ADDR']}' AND dateline>'".(TIME_NOW - (7 * 24 * 3600))."'";
		$query = $this->db->simple_select("myapi_sessions", "*", $where);
		if($this->db->num_rows($query) != 1)
		{
			return false;
		}

		$session = $this->db->fetch_array($query);

		return $session;
	}

	/**
	 * Set the request
	 *
	 * @param array $request The request array.
	 */
	public function setRequest($request)
	{
		$this->request = $request;
	}

	/**
	 * Get the request
	 * @return mixed[] $array
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * Set the request method
	 *
	 * @param string The request method
	 */
	public function setMethod($method)
	{
		$this->method = strtolower($method);
	}

	/**
	 * Get the request method
	 * @return string Returns the request method
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * Set the response format
	 *
	 * @param string Method The HTTP request method
	 */
	public function setFormat($format)
	{
		if(empty($format))
		{
			$format = "json";
		}
		if(strtolower($format) != "json" && strtolower($format) != "xml")
		{
			$this->setStatus(400);
			$this->format = "json";
			
			return;
		}
		$this->format = strtolower($format);
	}

	/**
	 * Get the requested response format
	 * @return string Returns the requested format of the response
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * Set the request data
	 *
	 * @param mixed[] The request data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * Get the data passed as part of the request
	 * @return mixed[] Returns the request data
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Set the response
	 *
	 * @param string The response to the request
	 */
	public function setResponse($response)
	{
		$return = array();

		$return['status'] = $this->status;

		if(!in_array($this->status, $this->nonErrorStatuses))
		{
			if(isset($response['message']))
			{
				$return['error'] = array('message' => $response['message']);
			}
			else
			{
				$return['error'] = array('message' => $this->getStatusMessage($this->status));
			}
		}
		else
		{
			$return['response'] = $response;
		}

		if($this->getFormat() == 'json')
		{
			$this->response = json_encode($return);
		}
		else
		{
			$this->response = "<xml>\n".$this->xml_encode($return)."</xml>";
		}
	}

	/**
	 * Get the response
	 * @return mixed[] $array Returns the actual response
	 */
	public function getResponse()
	{
		return $this->response;
	}

	/**
	 * Send the HTML formatted response
	 * @return null
	 */
	public function sendResponse()
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

		if($this->getFormat() == 'json')
		{
			if($this->callback == "")
			{
				header('Content-type: application/json');
				echo $this->getResponse();
			}
			else
			{
				header('Content-type: application/javascript');
				echo $this->callback."('".$this->getResponse()."');";
			}
		}
		else
		{
			header('Content-type: application/xml');
			echo $this->getResponse();
		}
		exit;
	}

	/**
	 * Set the response status
	 *
	 * @param int Status The HTTP response code for the request
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * Get the response status
	 * @return int Returns the HTTP response code for the request
	 */
	public function getStatus()
	{
		return $this->status;
	}
	
	public function xml_encode($array, $tabs="")
	{
		$return = "";
		foreach($array as $key => $val)
		{
			$ckey = $key;
			if(!is_string($key))
			{
				$key = "element id=\"{$key}\"";
				$ckey = "element";
			}
			if(is_array($val))
			{
				$return .= "{$tabs}<{$key}>\n".$this->xml_encode($val, $tabs." ")."</{$ckey}>\n";
			}
			else
			{
				$val = '<![CDATA['
					.str_replace(']]>', ']]]]><![CDATA[>', $val)
					.']]>';
				$return .= "{$tabs}<{$key}>{$val}</{$ckey}>\n";
			}
		}
		return $return;
	}
	
	//Source: http://www.geekality.net/2010/06/27/php-how-to-easily-provide-json-and-jsonp/
	public function is_valid_callback($callback)
	{
		 $identifier_syntax = '/^[$_\p{L}][$_\p{L}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\x{200C}\x{200D}]*+$/u';

		 $reserved_words = array('break', 'do', 'instanceof', 'typeof', 'case',
		   'else', 'new', 'var', 'catch', 'finally', 'return', 'void', 'continue', 
		   'for', 'switch', 'while', 'debugger', 'function', 'this', 'with', 
		   'default', 'if', 'throw', 'delete', 'in', 'try', 'class', 'enum', 
		   'extends', 'super', 'const', 'export', 'import', 'implements', 'let', 
		   'private', 'public', 'yield', 'interface', 'package', 'protected', 
		   'static', 'null', 'true', 'false');

		 return preg_match($identifier_syntax, $callback) && !in_array(mb_strtolower($callback, 'UTF-8'), $reserved_words);
	}

	/**
	 * Get the response status label
	 *
	 * @param int Status The HTTP status code
	 *
	 * @return string Returns the HTTP response code for the request
	 */
	public function getStatusMessage($status)
	{
		$codes = Array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported'
		);

		return (isset($codes[$status])) ? $codes[$status] : '';
	}
}
