<?php
/**
 * Sesion handler API v1 (based on MyBB 1.6 source which is licensed un der LGPL)
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
class session
{
	public $sid = 0;
	public $uid = 0;
	public $ipaddress = '';
	public $useragent = '';
	public $is_spider = false;

	/**
	 * Initialize a session
	 */
	function init()
	{
		global $db, $mybb, $cache;

		// Get our visitor's IP.
		$this->ipaddress = get_ip();

		// Find out the user agent. (Don't really need it but MyBB wants it)
		$this->useragent = $_SERVER['HTTP_USER_AGENT'];
		if(my_strlen($this->useragent) > 100)
		{
			$this->useragent = my_substr($this->useragent, 0, 100);
		}
	}

	/**
	 * Load a user via the user credentials.
	 *
	 * @param int    The user id.
	 * @param string The user's password.
	 *
	 * @return bool
	 */
	function load_user($uid)
	{
		global $mybb, $db, $time, $lang, $mybbgroups, $session, $cache;

		// Read the banned cache
		$bannedcache = $cache->read("banned");

		// If the banned cache doesn't exist, update it and re-read it
		if(!is_array($bannedcache))
		{
			$cache->update_banned();
			$bannedcache = $cache->read("banned");
		}

		$uid        = intval($uid);
		$query      = $db->query("
			SELECT u.*, f.*
			FROM ".TABLE_PREFIX."users u
			LEFT JOIN ".TABLE_PREFIX."userfields f ON (f.ufid=u.uid)
			WHERE u.uid='$uid'
			LIMIT 1
		");
		$mybb->user = $db->fetch_array($query);

		if(!empty($bannedcache[$uid]))
		{
			$banned_user                          = $bannedcache[$uid];
			$mybb->user['bandate']                = $banned_user['dateline'];
			$mybb->user['banlifted']              = $banned_user['lifted'];
			$mybb->user['banoldgroup']            = $banned_user['oldgroup'];
			$mybb->user['banolddisplaygroup']     = $banned_user['olddisplaygroup'];
			$mybb->user['banoldadditionalgroups'] = $banned_user['oldadditionalgroups'];
		}

		// Check the password if we're not using a session
		if(!$mybb->user['uid'])
		{
			unset($mybb->user);
			$this->load_guest();
		}
		$this->uid = $mybb->user['uid'];

		// Sort out the private message count for this user.
		if(($mybb->user['totalpms'] == -1 || $mybb->user['unreadpms'] == -1) && $mybb->settings['enablepms'] != 0) // Forced recount
		{
			$update = 0;
			if($mybb->user['totalpms'] == -1)
			{
				$update += 1;
			}
			if($mybb->user['unreadpms'] == -1)
			{
				$update += 2;
			}

			require_once MYBB_ROOT."inc/functions_user.php";
			$pmcount = update_pm_count('', $update);
			if(is_array($pmcount))
			{
				$mybb->user = array_merge($mybb->user, $pmcount);
			}
		}
		$mybb->user['pms_total']  = $mybb->user['totalpms'];
		$mybb->user['pms_unread'] = $mybb->user['unreadpms'];

		if($mybb->user['lastip'] != $this->ipaddress && array_key_exists('lastip', $mybb->user))
		{
			$lastip_add = ", lastip='".$db->escape_string($this->ipaddress)."', longlastip='".intval(my_ip2long($this->ipaddress))."'";
		}
		else
		{
			$lastip_add = '';
		}

		// Sort out the language and forum preferences.
		if($mybb->user['language'] && $lang->language_exists($mybb->user['language']))
		{
			$mybb->settings['bblanguage'] = $mybb->user['language'];
		}
		if($mybb->user['dateformat'] != 0 && $mybb->user['dateformat'] != '')
		{
			global $date_formats;
			if($date_formats[$mybb->user['dateformat']])
			{
				$mybb->settings['dateformat'] = $date_formats[$mybb->user['dateformat']];
			}
		}

		// Choose time format.
		if($mybb->user['timeformat'] != 0 && $mybb->user['timeformat'] != '')
		{
			global $time_formats;
			if($time_formats[$mybb->user['timeformat']])
			{
				$mybb->settings['timeformat'] = $time_formats[$mybb->user['timeformat']];
			}
		}

		// Find out the threads per page preference.
		if($mybb->user['tpp'])
		{
			$mybb->settings['threadsperpage'] = $mybb->user['tpp'];
		}

		// Find out the posts per page preference.
		if($mybb->user['ppp'])
		{
			$mybb->settings['postsperpage'] = $mybb->user['ppp'];
		}

		// Does this user prefer posts in classic mode?
		if($mybb->user['classicpostbit'])
		{
			$mybb->settings['postlayout'] = 'classic';
		}
		else
		{
			$mybb->settings['postlayout'] = 'horizontal';
		}

		// Check if this user is currently banned and if we have to lift it.
		if(!empty($mybb->user['bandate']) && (isset($mybb->user['banlifted']) && !empty($mybb->user['banlifted'])) && $mybb->user['banlifted'] < $time) // hmmm...bad user... how did you get banned =/
		{
			// must have been good.. bans up :D
			$db->shutdown_query("UPDATE ".TABLE_PREFIX."users SET usergroup='".intval($mybb->user['banoldgroup'])."', additionalgroups='".$mybb->user['oldadditionalgroups']."', displaygroup='".intval($mybb->user['olddisplaygroup'])."' WHERE uid='".$mybb->user['uid']."' LIMIT 1");
			$db->shutdown_query("DELETE FROM ".TABLE_PREFIX."banned WHERE uid='".$mybb->user['uid']."'");
			// we better do this..otherwise they have dodgy permissions
			$mybb->user['usergroup']        = $mybb->user['banoldgroup'];
			$mybb->user['displaygroup']     = $mybb->user['banolddisplaygroup'];
			$mybb->user['additionalgroups'] = $mybb->user['banoldadditionalgroups'];
			$cache->update_banned();

			$mybbgroups = $mybb->user['usergroup'];
			if($mybb->user['additionalgroups'])
			{
				$mybbgroups .= ','.$mybb->user['additionalgroups'];
			}
		}
		else if(!empty($mybb->user['bandate']) && (empty($mybb->user['banlifted']) || !empty($mybb->user['banlifted']) && $mybb->user['banlifted'] > $time))
		{
			$mybbgroups = $mybb->user['usergroup'];
		}
		else
		{
			// Gather a full permission set for this user and the groups they are in.
			$mybbgroups = $mybb->user['usergroup'];
			if($mybb->user['additionalgroups'])
			{
				$mybbgroups .= ','.$mybb->user['additionalgroups'];
			}
		}

		$mybb->usergroup = usergroup_permissions($mybbgroups);
		if(!$mybb->user['displaygroup'])
		{
			$mybb->user['displaygroup'] = $mybb->user['usergroup'];
		}

		$mydisplaygroup = usergroup_displaygroup($mybb->user['displaygroup']);
		if(is_array($mydisplaygroup))
		{
			$mybb->usergroup = array_merge($mybb->usergroup, $mydisplaygroup);
		}

		if(!$mybb->user['usertitle'])
		{
			$mybb->user['usertitle'] = $mybb->usergroup['usertitle'];
		}

		return true;
	}

	/**
	 * Load a guest user.
	 */
	function load_guest()
	{
		global $mybb, $time, $db, $lang;

		// Set up some defaults
		$time                       = TIME_NOW;
		$mybb->user['usergroup']    = 1;
		$mybb->user['username']     = '';
		$mybb->user['uid']          = 0;
		$mybbgroups                 = 1;
		$mybb->user['displaygroup'] = 1;

		// Gather a full permission set for this guest
		$mybb->usergroup = usergroup_permissions($mybbgroups);
		$mydisplaygroup  = usergroup_displaygroup($mybb->user['displaygroup']);

		$mybb->usergroup = array_merge($mybb->usergroup, $mydisplaygroup);
	}
}