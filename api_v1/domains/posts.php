<?php
/**
 * Handles post related requests to API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
if(!defined('IN_API'))
{
	die('Direct initialization is not allowed.');
}

/**
 * Posts class
 * @api
 * @version 1.0
 */
class posts extends myAPI
{

	private $noAuthReqd = "pid,tid,replyto,fid,subject,icon,uid,username,dateline,message,includesig,smilieoff,edituid,edittime";

	/**
	 * Class constructor
	 *
	 * @param            $request
	 * @param \MyBB      $mybbIn  Our MyBB object.
	 * @param            $dbIn
	 * @param \datacache $cacheIn Our cache oject.
	 */
	public function posts($request, MyBB $mybbIn, $dbIn, datacache $cacheIn)
	{
		parent::__construct($request, $mybbIn, $dbIn, $cacheIn);
	}

	/**
	 * Fetch details about a single post.
	 *
	 * @param int $id The posts's pid.
	 *
	 * @return object $post An object representing a single post.
	 */
	public function getPosts($id = 0)
	{
		$id = (int)$id;
		if($id == 0)
		{
			$this->setStatus(404);

			exit;
		}
		$query = $this->db->simple_select('posts', '*', "pid = {$id}", array('limit' => 1));
		if($this->db->num_rows($query) != 1)
		{
			$this->setStatus(404);

			exit;
		}
		$post = $this->db->fetch_array($query);

		$post = (object)$post;

		//format $post
		$post->pid        = (int)$post->pid;
		$post->tid        = (int)$post->tid;
		$post->replyto    = (int)$post->replyto;
		$post->fid        = (int)$post->fid;
		$post->subject    = htmlspecialchars($post->subject);
		$post->icon       = (int)$post->icon;
		$post->uid        = (int)$post->uid;
		$post->username   = htmlspecialchars($post->username);
		$post->dateline   = new DateTime('@'.$post->dateline);
		$post->message    = htmlspecialchars($post->message);
		$post->includesig = (bool)$post->includesig;
		$post->smilieoff  = (bool)$post->smilieoff;
		$post->edituid    = (int)$post->edituid;
		$post->edittime   = ((int)$post->edittime == 0) ? 0 : new DateTime('@'.$post->edittime);
		$post->visible    = (bool)$post->visible;

		$this->setStatus(200);

		return $post;
	}

	public function setPosts($id = 0)
	{
		//with PID: update a post
		//without PID: create a post
	}

	public function setDelete($id = 0)
	{
		//delete a post
	}

	public function getLatest($id = 0)
	{
		//gets limited/full post details for last x posts
	}
}