<?php
/**
 * Handles user related requests to API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
if(!defined('IN_API'))
{
	die('Direct initialization is not allowed.');
}

/**
 * Users class
 * @api
 * @version 1.0
 */
class users extends myAPI
{
	private $noAuthReqd = "uid,username,avatar,avatardimensions,avatartype,usergroup,additionalgroups,displaygroup,usertitle,regdate,website,aim,icq,yahoo,msn,signature";

	/**
	 * Class constructor
	 *
	 * @param            $request
	 * @param \MyBB      $mybbIn  Our MyBB object.
	 * @param            $dbIn
	 * @param \datacache $cacheIn Our cache object.
	 */
	public function users($request, MyBB $mybbIn, $dbIn, datacache $cacheIn)
	{
		parent::__construct($request, $mybbIn, $dbIn, $cacheIn);
	}

	/**
	 * Get the requested user(s)
	 *
	 * @param int Id The Id of the user to lookup
	 *
	 * @return array Returns and array of user arrays
	 */
	public function getUsers($id = 0)
	{
		if($id == 0)
		{
			$where = '1=1';
		}
		elseif(is_numeric($id))
		{
			$where = 'uid='.(int)$id;
		}
		else
		{
			$id    = $this->db->escape_string(trim($id));
			$where = "username = '{$id}'";
		}

		if($this->isauth == 1)
		{
			$fields = '*';
		}
		else
		{
			$fields = $this->noAuthReqd;
		}

		$query  = $this->db->simple_select('users', $fields, $where, array("limit_start" => $this->start, "limit" => $this->per_page));
		$result = array();
		while($row = $this->db->fetch_array($query))
		{
			$result[$row['uid']] = $row;
		}

		if(count($result) == 0)
		{
			$this->setStatus(404);
		}

		return $result;
	}

	public function setUsers($uid = 0)
	{
		require_once(MYBB_ROOT.'inc/functions_user.php');
		require_once(MYBB_ROOT.'inc/datahandlers/user.php');

		$user = $this->data;
		unset($user['token']);

		if(count($user) == 0)
		{
			$this->setStatus(400);

			return 0;
		}

		if($uid > 0)
		{
			if(!isset($user['uid']))
			{
				$user['uid'] = $uid;
			}
			$userhandler = new UserDataHandler('update');
			$userhandler->set_data($user);
			if(!$userhandler->validate_user())
			{
				$this->setStatus(400);

				return array('message' => $userhandler->errors);
			}
			$userhandler->update_user();
		}
		else
		{
			unset($user['uid']);
			$userhandler = new UserDataHandler('insert');
			$userhandler->set_data($user);
			if(!$userhandler->validate_user())
			{
				$this->setStatus(400);

				return array('message' => $userhandler->errors);
			}
			$userhandler->insert_user();
		}

		return $this->getUsers($userhandler->uid);
	}

	public function setDelete($uid = 0)
	{
		if($uid <= 0)
		{
			$this->setStatus(400);

			return 0;
		}

		$user = get_user($uid);

		if(!$user['uid'])
		{
			//The user doesn't exist
			$this->setStatus(404);

			return 0;
		}

		if(is_super_admin($uid) || $this->mybb->user['uid'] == $uid)
		{
			$this->setStatus(403);

			return 0;
		}

		//Now delete everything...
		$this->db->delete_query("userfields", "ufid='{$user['uid']}'");
		$this->db->delete_query("privatemessages", "uid='{$user['uid']}'");
		$this->db->delete_query("events", "uid='{$user['uid']}'");
		$this->db->delete_query("forumsubscriptions", "uid='{$user['uid']}'");
		$this->db->delete_query("threadsubscriptions", "uid='{$user['uid']}'");
		$this->db->delete_query("sessions", "uid='{$user['uid']}'");
		$this->db->delete_query("banned", "uid='{$user['uid']}'");
		$this->db->delete_query("threadratings", "uid='{$user['uid']}'");
		$this->db->delete_query("users", "uid='{$user['uid']}'");
		$this->db->delete_query("joinrequests", "uid='{$user['uid']}'");
		$this->db->delete_query("warnings", "uid='{$user['uid']}'");
		$this->db->delete_query("reputation", "uid='{$user['uid']}' OR adduid='{$user['uid']}'");
		$this->db->delete_query("awaitingactivation", "uid='{$user['uid']}'");
		$this->db->delete_query("posts", "uid = '{$user['uid']}' AND visible = '-2'");
		$this->db->delete_query("threads", "uid = '{$user['uid']}' AND visible = '-2'");

		// Update forum stats
		update_stats(array('numusers' => '-1'));

		// Update forums & threads if user is the lastposter
		$this->db->update_query("posts", array('uid' => 0), "uid='{$user['uid']}'");
		$this->db->update_query("forums", array("lastposteruid" => 0), "lastposteruid = '{$user['uid']}'");
		$this->db->update_query("threads", array("lastposteruid" => 0), "lastposteruid = '{$user['uid']}'");

		// Did this user have an uploaded avatar?
		if($user['avatartype'] == "upload")
		{
			// Removes the ./ at the beginning the timestamp on the end...
			@unlink("../".substr($user['avatar'], 2, -20));
		}

		// Was this user a moderator?
		if(is_moderator($user['uid']))
		{
			$this->db->delete_query("moderators", "id='{$user['uid']}' AND isgroup = '0'");
			$this->cache->update_moderators();
		}

		//Ok we did it
		return 1;
	}

	/**
	 * Login user
	 *
	 * @param array $arg The user/pass.
	 *
	 * @return mixed[] $array Return user upon success
	 */
	public function setLogin($arg)
	{
		$login = $this->data;
		require_once MYBB_ROOT."inc/functions_user.php";
		$login_data = validate_password_from_username($login['username'], $login['password']);
		if(!$login_data)
		{
			$this->setStatus(401);
		}
		else
		{
			$token = random_str(50);
			$this->db->insert_query("myapi_sessions", array("uid" => $login_data['uid'], "token" => $token, "ip" => $_SERVER['REMOTE_ADDR'], "dateline" => TIME_NOW));

			$login_data['usertoken'] = $token;

			return $login_data;
		}
	}

	public function getLogin($token = 0)
	{
		if($this->isauth == 0)
		{
			$this->setStatus(401);

			return 0;
		}

		if($token === 0)
		{
			//Probably it's passed as usertoken?
			$token = $this->data['usertoken'];
		}
		$session = parent::getLoggedin($token);

		if(!$session)
		{
			$this->setStatus(400);
		}
		else
		{
			return $this->getUsers($session['uid']);
		}
	}
}