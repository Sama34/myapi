#MyAPI for MyBB

###Description

MyAPI is a RESTful API for interacting with a MyBB forum installation. It is based on MyBB 1.6 and will likely need to be updated to support MyBB 1.8

MyAPI is released under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (CC-BY-NC-SA-4.0)